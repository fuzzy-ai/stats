stats
=====

This is the stats microservice for Fuzzy.ai. It's responsible for managing logs
of usage data in the application.

Environment
-----------

It's a sub-class of [https://github.com/fuzzy-ai/microservice](Microservice), so
it uses all the same environment variables for basic configuration. It also
uses:

* `PERIOD`: Some database operations are queued up and sent all at once
  to limit impact on the server. This is the default period in ms for these
  operations. Defaults to 10 seconds.
* `INCREMENT_PERIOD`: We keep counts of number of evaluations per user and
  per agent by month and by day. We boxcar the increments for this, and do
  the db calls periodically. This sets the period in milliseconds;
  default is value of `PERIOD`.
* `PREPEND_PERIOD`: Logs of every evaluation are stored in a stream in the
  database. They are boxcarred and updated periodically. This sets the period in
  milliseconds; default is value of `PERIOD`.
* `TRIM_PERIOD`: Logs of every evaluation are stored in a stream in the
  database. They are periodically trimmed. This sets the period in milliseconds;
  default is value of `PERIOD`.

Endpoints
---------

The server has the following endpoints. All of them require OAuth2 for
authentication, using only app authentication keys.

All of the endpoints take JSON payloads.

* GET /version

  Gets the current version information for the service. This requires no
  authentication.

* GET /live

  Returns `{"status": "OK"}`. This is a good endpoint
  to hit if you're testing whether the server is up at all.

  This requires no authentication.

* GET /ready

  Tries to connect to the database then returns `{"status": "OK"}`. This is a
  good endpoint to hit if you're testing whether the server is ready to take
  real traffic.

  This requires no authentication.

* GET /user/:userID/:year/:month

  * userID: ID of the user
  * year: 4-digit year to get stats for
  * month: 2-digit month to get stats for

  Gets the user's API usage data for the given month. Requires a user access
  token.

  Returns a JSON object with the following properties:

  * total: total number of API calls made during this month
  * byDay: a map of usage data per day of month, with keys formatted as
    "YYYY-MM-DD" and values as integers

* GET /user/:userID/:year/:month/usage

  * userID: ID of the user
  * year: 4-digit year to get stats for
  * month: 2-digit month to get stats for

  Gets the user's API total usage for the given month. Requires a user access
  token.

  Returns a JSON integer for the total usage.

* GET /agent/:agentID/:year/:month

  * agentID: ID of the agent
  * year: 4-digit year to get stats for
  * month: 2-digit month to get stats for

  Gets the user's API usage data for the given month for a single agent.
  Requires a user access token.

  Returns a JSON object with the following properties:

  * total: total number of API calls made during this month to this agent
  * byDay: a map of usage data for the agent per day of month, with keys
    formatted as "YYYY-MM-DD" and values as integers.

* GET /user/:userID/stream?offset=<n>&limit=<n>

  * userID: ID of the user
  * offset: start with this index (0-based), default = 0
  * limit: return this many evaluations maximum, default = 20

  Gets a reverse-chronological (newest first) stream of evaluations for the
  user. Requires a user access token. `offset` and `limit` are optional
  parameters used for paging.

  Returns a JSON array of EvaluationLog objects. Each EvaluationLog object has
  the following properties:

  * reqID: unique ID for the evaluation
  * userID: ID of the user who performed the evaluation; should be identical
    to the userID requested
  * agentID: ID of the agent for the evaluation.
  * versionID: ID for the version of the agent. If an agent is changed, it will
    get a new version, which may give different results.
  * input: Object representing the inputs for the evaluation, mapping input name
    to input value. For example, `{"temperature": 25, "pressure": 100.0}`
  * fuzzified: fuzzified input values. Maps each input name to an object which
    maps a fuzzy set name to a membership value. For example,
    `{"temperature": {"cold": 0.1, "warm": 0.4, "hot": 0.5},
      "pressure": {"low": 0.2, "medium": 0.7, "high": 0.1}}`
  * rules: Array of integers representing the indices of rules in the agent's
    `rules` array that fired as part of this evaluation.
  * inferred: fuzzified output values. Maps each output name to an object which
    maps a fuzzy set name to a membership value. For example,
    `{"volume": {"small": 0.1, "medium": 0.4, "large": 0.5}}`
  * clipped: Map of output sets clipped at the membership value from `inferred`.
    Object maps each output name to an object that maps each set name to a
    polygon representing the clipped set. Each polygon is a 3- or 4-member array
    of 2-member arrays, representing the `x, y` coordinates of the vertices of
    the polygon. The points are in left-to-right order, from the lowest-x point
    to the highest-x point. For example,
    `{"volume": {"small": [[0.0, 0.0], [0.2, 0.1], [0.4, 0.1], [0.6, 0.0]],
                 "medium": [[0.5, 0.0], [0.7, 0.4], [0.9, 0.4], [1.1, 0.0]],
                 "large": [[1.0, 0.0], [1.3, 0.5], [1.5, 0.5], [1.7, 0.0]]}}`
  * combined: Map of output names to polygons that have been combined from each
    fuzzy set. Maps output name to a polygon, which is an array of 2-member
    arrays, each representing a vertex in the polygon. For example,
    `{"volume": [ [ 0, 0 ],
                  [ 0.2, 0.1 ],
                  [ 0.4, 0.1 ],
                  [ 0.52, 0.04000000000000004 ],
                  [ 0.7, 0.4 ],
                  [ 0.9, 0.4 ],
                  [ 1.0545454545454545, 0.09090909090909075 ],
                  [ 1.3, 0.5 ],
                  [ 1.5, 0.5 ],
                  [ 1.7, 0 ] ] ]}`
  * centroid: Map of outputs names to points representing the centroid of the
    polygon in the `combined` property. Each point is a 2-member array. For
    example, `{"volume": [ 1.0573997195504907, 0.17684658885180282 ]}`
  * crisp: Crisp value of the defuzzified outputs. This is the "output" that is
    returned to the user in the evaluation. An object mapping the output name to
    the defuzzified value, for example: `{"volume": 1.0573997195504907}`.
  * createdAt: ISO 8601 timestamp for the evaluation. Note that EvaluationLog
    objects are immutable; they happened once.

* GET /agent/:agentID/stream?offset=<n>&limit=<n>

  * agentID: ID of the agent
  * offset: start with this index (0-based), default = 0
  * limit: return this many evaluations maximum, default = 20

  Gets a reverse-chronological (newest first) stream of evaluations for the
  agent. Requires a user access token. `offset` and `limit` are optional
  parameters used for paging.

  Returns a JSON array of EvaluationLog objects, as defined above.

* POST /evaluation

  Creates a new EvaluationLog object, as defined above. Requires a user access
  token.

  Returns the EvaluationLog object with `createdAt` timestamp added.

* GET /evaluation/:reqID

  Returns an EvaluationLog with the given `reqID` unique ID. Requires a user
  access token.

  Returns the EvaluationLog object.
