// agent.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const db = require('databank')

const AgentVersion = require('./agentversion')

const Agent = db.DatabankObject.subClass('Agent')

Agent.schema = {
  pkey: 'id',
  fields: [
    'userID',
    'latestVersion',
    'name',
    'createdAt',
    'updatedAt'
  ],
  indices: ['userID']
}

Agent.beforeCreate = (props, callback) => callback(new Error('Read-only'))

Agent.prototype.beforeUpdate = (props, callback) => callback(new Error('Read-only'))

Agent.prototype.beforeSave = callback => callback(new Error('Read-only'))

Agent.prototype.beforeDel = callback => callback(new Error('Read-only'))

Agent.prototype.afterGet = function (callback) {
  if (this.latestVersion != null) {
    return AgentVersion.get(this.latestVersion, (err, ver) => {
      if (err) {
        return callback(err)
      } else {
        this.properties = ver.properties
        return callback(null)
      }
    })
  } else {
    return callback(null)
  }
}

Agent.prototype.afterCreate = Agent.prototype.afterGet
Agent.prototype.afterUpdate = Agent.prototype.afterGet

module.exports = Agent
