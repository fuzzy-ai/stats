// statsserver.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const assert = require('assert')

const Microservice = require('@fuzzy-ai/microservice')
const _ = require('lodash')
const async = require('async')
const {DatabankObject} = require('databank')
const debug = require('debug')('stats:statsserver')

const version = require('./version')
const HTTPError = require('./httperror')
const User = require('./user')
const EvaluationLog = require('./evaluationlog')
const Agent = require('./agent')
const AgentVersion = require('./agentversion')
const MonthlyStats = require('./monthly-stats')
const Feedback = require('./feedback')

const MAX_STREAM_LENGTH = 256

const lpad = function (value, padding) {
  let zeroes = '0'
  for (let i = 1, end = padding, asc = end >= 1; asc ? i <= end : i >= end; asc ? i++ : i--) { zeroes += '0' }

  return (zeroes + value).slice(padding * -1)
}

const daysInMonth = (month, year) => new Date(year, month, 0).getDate()

const withoutMetaCruft = function (log) {
  const cleaned = _.clone(log)
  cleaned.crisp = _.omit(cleaned.crisp, _.filter(_.keys(cleaned.crisp), key => _.isObject(cleaned.crisp[key])))
  return cleaned
}

class StatsServer extends Microservice {
  constructor (...args) {
    super(...args)
    this.periodicTrimmer = this.periodicTrimmer.bind(this)
    this.periodicPrepender = this.periodicPrepender.bind(this)
    this.periodicIncrementer = this.periodicIncrementer.bind(this)
  }

  environmentToConfig (env) {
    const cfg = super.environmentToConfig(env)
    cfg.period = this.envInt(env, 'PERIOD', 10000)
    cfg.trimPeriod = this.envInt(env, 'TRIM_PERIOD', cfg.period)
    cfg.incrementPeriod = this.envInt(env, 'INCREMENT_PERIOD', cfg.period)
    cfg.prependPeriod = this.envInt(env, 'PREPEND_PERIOD', cfg.period)
    return cfg
  }

  setupParams (exp) {
    exp.param('userID', (req, res, next, id) => {
      debug(`Getting user for ${id}`)
      return User.get(id, (err, user) => {
        if (err) {
          debug(`Error getting user: ${err}`)
          return next(err)
        } else {
          debug('Got user')
          debug(user)
          req.user = user
          return next()
        }
      })
    })

    exp.param('agentID', (req, res, next, id) =>
      Agent.get(id, (err, agent) => {
        if (err) {
          return next(err)
        } else {
          req.agent = agent
          return next()
        }
      })
    )

    exp.param('evaluationID', (req, res, next, id) => {
      debug(`Getting evaluation with id ${id}`)
      return EvaluationLog.get(id, (err, evaluationLog) => {
        if (err) {
          debug(`${err.name} getting evaluation with id ${id}: ${err.message}`)
          return next(err)
        } else {
          debug(`success getting evaluation with id ${id}`)
          req.evaluation = evaluationLog
          return next()
        }
      })
    })

    return exp
  }

  setupRoutes (exp) {
    let body
    exp.get('/version', (req, res, next) => res.json({name: 'stats', version}))

    exp.get('/live', this.dontLog, (req, res, next) => res.json({status: 'OK'}))

    exp.get('/ready', this.dontLog, (req, res, next) => {
      if ((this.db == null)) {
        return next(new HTTPError('Database not connected', 500))
      }

      return this.db.save('server-ready', 'stats', 1, (err, readiness) => {
        if (err != null) {
          return next(err)
        } else {
          return res.json({status: 'OK'})
        }
      })
    })

    // XXX: This is really complicated to get good performance for reads
    // and writes.

    exp.get('/user/:userID/:year/:month', (req, res, next) => {
      // FIXME
      let err, month
      const db = DatabankObject.bank

      const readMonth = function (id, year, month, callback) {
        const mkey = `${id}-${lpad(year, 4)}-${lpad(month, 2)}`
        debug(mkey)
        return db.read('monthlyUsage', mkey, (err, usage) => {
          if (err && (err.name === 'NoSuchThingError')) {
            debug('Returning zero for empty usage')
            return callback(null, 0)
          } else if (err) {
            debug(`Returning error ${err}`)
            return callback(err)
          } else {
            debug(`Returning usage ${usage}`)
            return callback(null, usage)
          }
        })
      }

      const readDayFull = function (id, year, month, day, callback) {
        const dkey = `${id}-${lpad(year, 4)}-${lpad(month, 2)}-${lpad(day, 2)}`
        return db.read('dailyUsage', dkey, (err, usage) => {
          if (err && (err.name === 'NoSuchThingError')) {
            return callback(null, 0)
          } else if (err) {
            return callback(err)
          } else {
            return callback(null, usage)
          }
        })
      }

      const freshStats = function (id, year, month, callback) {
        debug(`Getting fresh stats for ${id}, ${year}, ${month}`)
        let maxDay = null
        return async.parallel([
          function (callback) {
            debug(`Reading month for ${id}, ${year}, ${month}`)
            return readMonth(id, year, month, callback)
          },
          function (callback) {
            const readDay = function (day, callback) {
              debug(`Reading day for ${id}, ${year}, ${month}, ${day}`)
              return readDayFull(id, year, month, day, callback)
            }

            const now = new Date()
            const nyear = now.getUTCFullYear()
            const nmonth = now.getUTCMonth() + 1

            if ((nyear === year) && (nmonth === month)) {
              maxDay = now.getUTCDate()
            } else {
              maxDay = daysInMonth(year, month)
            }

            debug(`maxDay = ${maxDay}`)

            return async.map(__range__(1, maxDay, true), readDay, callback)
          }
        ], (err, results) => {
          if (err) {
            return callback(err)
          } else {
            const props = {total: results[0]}
            const keys = _.map(__range__(1, maxDay, true), day => `${lpad(year, 4)}-${lpad(month, 2)}-${lpad(day, 2)}`)
            props.byDay = _.zipObject(keys, results[1])
            if (maxDay < daysInMonth(year, month)) {
              for (let start = maxDay + 1, day = start, end = daysInMonth(year, month), asc = start <= end; asc ? day <= end : day >= end; asc ? day++ : day--) {
                const key = `${lpad(year, 4)}-${lpad(month, 2)}-${lpad(day, 2)}`
                props.byDay[key] = 0
              }
            }
            props.id = id
            props.year = year
            props.month = month
            debug('initializing stats from props')
            debug(props)
            const stats = new MonthlyStats(props)
            debug(stats)
            setImmediate(() => {
              debug('Saving fresh stats')
              debug(stats)
              return stats.save((err) => {
                debug(err)
                if (err) {
                  return req.log.error({err}, 'Error saving stats')
                }
              })
            })
            return callback(null, stats)
          }
        })
      }

      const refreshStats = function (stats, id, year, month, callback) {
        let maxDay, monthEnd
        const updatedAt = new Date(stats.updatedAt)

        if (month === 12) {
          monthEnd = new Date(year + 1, 0)
        } else {
          // zero-indexed, but next month.
          monthEnd = new Date(year, (month + 1) - 1)
        }

        if (updatedAt.getTime() >= monthEnd.getTime()) {
          return callback(null, stats)
        }

        let minDay = (maxDay = null)

        return async.parallel([
          callback => readMonth(id, year, month, callback),
          function (callback) {
            const readDay = function (day, callback) {
              debug(`Reading day for ${id}, ${year}, ${month}, ${day}`)
              return readDayFull(id, year, month, day, callback)
            }

            const uyear = updatedAt.getUTCFullYear()
            const umonth = updatedAt.getUTCMonth() + 1

            if ((uyear === year) && (umonth === month)) {
              minDay = updatedAt.getUTCDate()
            } else {
              minDay = 1
            }

            const now = new Date()
            const nyear = now.getUTCFullYear()
            const nmonth = now.getUTCMonth() + 1

            if ((nyear === year) && (nmonth === month)) {
              maxDay = now.getUTCDate()
            } else {
              maxDay = daysInMonth(year, month)
            }

            debug(`minDay = ${minDay}, maxDay = ${maxDay}`)

            return async.map(__range__(minDay, maxDay, true), readDay, callback)
          }
        ], (err, results) => {
          if (err) {
            return callback(err)
          } else {
            [stats.total] = results
            // For the calculated days, update the modu
            const keys = _.map(__range__(minDay, maxDay, true), day => `${lpad(year, 4)}-${lpad(month, 2)}-${lpad(day, 2)}`)
            _.assign(stats.byDay, _.zipObject(keys, results[1]))
            // Pad the rest of the month with zeroes
            if (maxDay < daysInMonth(year, month)) {
              for (let start = maxDay + 1, day = start, end = daysInMonth(year, month), asc = start <= end; asc ? day <= end : day >= end; asc ? day++ : day--) {
                const key = `${lpad(year, 4)}-${lpad(month, 2)}-${lpad(day, 2)}`
                stats.byDay[key] = 0
              }
            }
            setImmediate(() => {
              debug('Saving stats')
              debug(stats)
              return stats.save((err) => {
                if (err) {
                  return req.log.error({err}, 'Error saving stats')
                }
              })
            })
            return callback(null, stats)
          }
        })
      }

      let year = (month = 0)
      const now = new Date()
      const nyear = now.getUTCFullYear()
      const nmonth = now.getUTCMonth() + 1
      try {
        year = parseInt(req.params.year, 10)
        month = parseInt(req.params.month, 10)
      } catch (error) {
        err = error
        return next(err)
      }

      if (year < 2014) {
        return next(new Error(`No such year ${year}`, 404))
      }

      if ((month < 1) || (month > 12)) {
        return next(new Error(`No such month ${month}`, 404))
      }

      if ((year > nyear) || ((year === nyear) && (month > nmonth))) {
        return next(new Error(`${year}-${month} is in the future`, 400))
      }

      try {
        assert(_.isObject(req.user), `req.user is not an object: ${req.user}`)
        assert(_.isString(req.user.id), `req.user.id not string: ${req.user.id}`)
      } catch (error1) {
        err = error1
        return next(err)
      }

      const { id } = req.user

      debug(`id = ${id}, year = ${year}, month = ${month}`)

      return async.waterfall([
        function (callback) {
          debug('Getting cached stats')
          return MonthlyStats.maybeGet(id, year, month, callback)
        },
        function (stats, callback) {
          if (stats != null) {
            debug('Have cached stats; refreshing')
            return refreshStats(stats, id, year, month, callback)
          } else {
            debug('No cached stats; creating')
            return freshStats(id, year, month, callback)
          }
        }
      ], (err, stats) => {
        if (err) {
          return next(err)
        } else {
          const results = _.pick(stats, ['total', 'byDay'])
          debug(results)
          return res.json(results)
        }
      })
    })

    exp.get('/user/:userID/:year/:month/usage', (req, res, next) => {
      let month
      let year = (month = 0)
      const now = new Date()

      try {
        year = parseInt(req.params.year, 10)
        month = parseInt(req.params.month, 10)
      } catch (error) {
        const err = error
        return next(err)
      }

      if (year < 2014) {
        return next(new Error(`No such year ${year}`, 404))
      }

      if ((month < 1) || (month > 12)) {
        return next(new Error(`No such month ${month}`, 404))
      }

      const nowYear = now.getUTCFullYear()
      const nowMonth = now.getUTCMonth() + 1
      if ((year > nowYear) || ((year === nowYear) && (month > nowMonth))) {
        return next(new Error(`${year}-${month} is in the future`, 400))
      }

      // FIXME

      const db = DatabankObject.bank

      const key = `${req.user.id}-${lpad(year, 4)}-${lpad(month, 2)}`

      return db.read('monthlyUsage', key, (err, usage) => {
        if (err && (err.name === 'NoSuchThingError')) {
          return res.json(0)
        } else if (err) {
          return next(err)
        } else {
          return res.json(usage)
        }
      })
    })

    exp.get('/agent/:agentID/:year/:month', (req, res, next) => {
      let month
      let year = (month = 0)
      const now = new Date()

      try {
        year = parseInt(req.params.year, 10)
        month = parseInt(req.params.month, 10)
      } catch (error) {
        const err = error
        return next(err)
      }

      if (year < 2014) {
        return next(new Error(`No such year ${year}`, 404))
      }

      if ((month < 1) || (month > 12)) {
        return next(new Error(`No such month ${month}`, 404))
      }

      if ((year > now.getUTCFullYear()) || ((year === now.getUTCFullYear()) && (month > (now.getUTCMonth() + 1)))) {
        return next(new Error(`${year}-${month} is in the future`, 400))
      }

      const maxDay = daysInMonth(year, month)

      // FIXME
      const db = DatabankObject.bank

      return async.parallel([
        callback =>
          db.read('monthlyAgentUsage', `${req.agent.id}-${lpad(year, 4)}-${lpad(month, 2)}`, (err, usage) => {
            if (err && (err.name === 'NoSuchThingError')) {
              return callback(null, 0)
            } else if (err) {
              return callback(err)
            } else {
              return callback(null, usage)
            }
          }),
        function (callback) {
          const readDay = (day, callback) =>
            db.read('dailyAgentUsage', `${req.agent.id}-${lpad(year, 4)}-${lpad(month, 2)}-${lpad(day, 2)}`, (err, usage) => {
              if (err && (err.name === 'NoSuchThingError')) {
                return callback(null, 0)
              } else if (err) {
                return callback(err)
              } else {
                return callback(null, usage)
              }
            })

          return async.map(__range__(1, maxDay, true), readDay, callback)
        }
      ], (err, results) => {
        if (err) {
          return next(err)
        } else {
          const response = {total: results[0]}
          const keys = _.map(__range__(1, maxDay, true), day => `${lpad(year, 4)}-${lpad(month, 2)}-${lpad(day, 2)}`)
          response.byDay = _.zipObject(keys, results[1])
          return res.json(response)
        }
      })
    })

    exp.get('/user/:userID/stream', (req, res, next) => {
      const db = DatabankObject.bank
      const limit = req.query.limit || 20
      const offset = req.query.offset || 0
      return async.waterfall([
        callback => db.slice('UserEvaluationLogStream', req.user.id, offset, limit, callback),
        (ids, callback) => EvaluationLog.readArray(ids, callback)
      ], (err, logs) => {
        if (err && (err.name === 'NoSuchThingError')) {
          return res.json([])
        } else if (err) {
          return next(err)
        } else {
          // We have to clean up some cruft that got in there.
          return res.json(_.map(logs, withoutMetaCruft))
        }
      })
    })

    exp.get('/agent/:agentID/stream', (req, res, next) => {
      const db = DatabankObject.bank
      const limit = req.query.limit || 20
      const offset = req.query.offset || 0
      return async.waterfall([
        callback => db.slice('AgentEvaluationLogStream', req.agent.id, offset, limit, callback),
        (ids, callback) => EvaluationLog.readArray(ids, callback)
      ], (err, logs) => {
        if (err && (err.name === 'NoSuchThingError')) {
          return res.json([])
        } else if (err) {
          return next(err)
        } else {
          // We have to clean up some cruft that got in there.
          return res.json(_.map(logs, withoutMetaCruft))
        }
      })
    })

    exp.post('/evaluation', (req, res, next) => {
      const { fields } = EvaluationLog.schema
      const { pkey } = EvaluationLog.schema
      body = withoutMetaCruft(_.pick(req.body, _.concat(fields, pkey)))
      return this.logQ.push(body, (err, log) => {
        if (err) {
          return next(err)
        } else {
          const now = new Date()
          const ym = now.toISOString().substr(0, 7)
          const ymd = now.toISOString().substr(0, 10)
          this.scheduleStream('UserEvaluationLogStream', log.userID, log.reqID)
          this.scheduleStream('AgentEvaluationLogStream', log.agentID, log.reqID)
          this.scheduleIncrement('monthlyUsage', `${log.userID}-${ym}`)
          this.scheduleIncrement('dailyUsage', `${log.userID}-${ymd}`)
          this.scheduleIncrement('monthlyAgentUsage', `${log.agentID}-${ym}`)
          this.scheduleIncrement('dailyAgentUsage', `${log.agentID}-${ymd}`)
          return res.json(log)
        }
      })
    })

    exp.get('/evaluation/:evaluationID', (req, res, next) => res.json(withoutMetaCruft(req.evaluation)))

    exp.post('/evaluation/:evaluationID/feedback', (req, res, next) => {
      const props = {
        id: req.id,
        data: req.body,
        evaluationLog: req.evaluation.reqID
      }
      return Feedback.create(props, (err, feedback) => {
        if (err) {
          return next(err)
        } else {
          this.scheduleStream('EvaluationFeedbackStream', feedback.evaluationLog, feedback.id)
          this.scheduleStream('AgentVersionFeedbackStream', req.evaluation.versionID, feedback.id)
          this.scheduleStream('AgentFeedbackStream', req.evaluation.agentID, feedback.id)
          _.each(feedback.data, (v, k) => {
            const vkey = `${req.evaluation.versionID}-${k}`
            this.scheduleStream('AgentVersionPerformanceStream', vkey, feedback.id)
            const akey = `${req.evaluation.agentID}-${k}`
            return this.scheduleStream('AgentPerformanceStream', akey, feedback.id)
          })
          // Old API just returned an OK status. This version gives
          // the full feedback but we leave the status in so it's backwards
          // compatible
          return res.json(_.extend({'status': 'OK'}, feedback))
        }
      })
    })

    return exp.get('/evaluation/:evaluationID/feedback', (req, res, next) => {
      debug(`Handler called to get feedback for ${req.evaluation.reqID}`)
      // FIXME: take offset, limit query parameters
      return async.waterfall([
        callback => {
          return this.db.read('EvaluationFeedbackStream', req.evaluation.reqID, (err, ids) => {
            if (err) {
              if (err.name === 'NoSuchThingError') {
                return callback(null, [])
              } else {
                return callback(err)
              }
            } else {
              return callback(null, ids)
            }
          })
        },
        (ids, callback) => Feedback.readArray(ids, callback)
      ], (err, feedbacks) => {
        if (err) {
          return next(err)
        } else {
          return res.json(feedbacks)
        }
      })
    })
  }

  // This is a mix of mixed caps and lowercase. Boo.

  getSchema () {
    const classes = [
      Agent,
      AgentVersion,
      EvaluationLog,
      Feedback,
      MonthlyStats,
      User
    ]

    const schema = {}

    for (const cls of Array.from(classes)) {
      schema[cls.type] = cls.schema
    }

    return schema
  }

  scheduleStream (type, id, value) {
    const map = this.toPrepend.get(type)
    if (!map.has(id)) {
      map.set(id, new Set())
    }
    return map.get(id).add(value)
  }

  scheduleIncrement (type, id, value) {
    let old
    if (value == null) { value = 1 }
    const map = this.toIncrement.get(type)
    if (!map.has(id)) {
      old = 0
    } else {
      old = map.get(id)
    }
    return map.set(id, old + value)
  }

  scheduleTrim (type, id) {
    return this.toTrim.get(type).add(id)
  }

  incrementAll (callback) {
    debug('incrementAll() called')
    debug(`@toIncrement = ${JSON.stringify(this.toIncrement)}`)

    const incrementByType = (type, callback) => {
      const incrementByID = (id, callback) => {
        debug(`Incrementing for type ${type} and id ${id}`)

        const value = map.get(id)
        map.delete(id)

        debug(`Incrementing by ${value} for type ${type} and id ${id}`)

        return this.db.incrBy(type, id, value, err => {
          if (err) {
            debug(`${err.name} increment for ${type} and ${id}: ${err.message}`)
            debug(`Adding ${value} back to queue for ${type} and ${id}`)
            this.scheduleIncrement(type, id, value)
            return callback(err)
          } else {
            debug(`Successful increment for ${type} and ${id}`)
            return callback(null)
          }
        })
      }

      debug(`Incrementing for type ${type}`)

      const map = this.toIncrement.get(type)
      const ids = Array.from(map.keys())

      debug(`Incrementing for ids ${JSON.stringify(ids)}`)

      return async.eachLimit(ids, 16, incrementByID, (err) => {
        if (err != null) {
          debug(`Err incrementing for type ${type}: ${err}`)
          return callback(err)
        } else {
          debug(`Done incrementing for type ${type}`)
          return callback(null)
        }
      })
    }

    const types = Array.from(this.toIncrement.keys())

    debug(`Incrementing for types ${JSON.stringify(types)}`)

    return async.each(types, incrementByType, callback)
  }

  prependAll (callback) {
    debug('prependAll() called')
    debug(`@toPrepend = ${JSON.stringify(this.toPrepend)}`)

    const prependByType = (type, callback) => {
      const prependByID = (id, callback) => {
        debug(`Prepending for type ${type} and id ${id}`)

        let set = map.get(id)
        const values = Array.from(set)

        // FIXME: Break up values into standard 256-uuid chunks

        debug(`Prepending ${values.length} values for type ${type} and id ${id}`)

        return this.db.prependAll(type, id, values, err => {
          if (err) {
            debug(`${err.name} prepend for ${type} and ${id}: ${err.message}`)
            return callback(err)
          } else {
            debug(`Successful prepend for ${type} and ${id}`)
            // Remove the values we prepended. Some new ones
            // may have been added!
            values.forEach(value => set.delete(value))
            if (set.size === 0) {
              debug(`No prepends left for ${type} and ${id}; removing`)
              set = null
              map.delete(id)
            }
            // Trim the stream after a batch prepend
            debug(`Scheduling trim for ${type} and ${id}`)
            this.scheduleTrim(type, id)
            return callback(null)
          }
        })
      }

      debug(`Prepending for type ${type}`)

      const map = this.toPrepend.get(type)
      const ids = Array.from(map.keys())

      debug(`Prepending for ids ${JSON.stringify(ids)}`)

      return async.eachLimit(ids, 16, prependByID, (err) => {
        if (err != null) {
          debug(`Err prepending for type ${type}: ${err}`)
          return callback(err)
        } else {
          debug(`Done prepending for type ${type}`)
          return callback(null)
        }
      })
    }

    const types = Array.from(this.toPrepend.keys())

    return async.each(types, prependByType, callback)
  }

  trimAll (callback) {
    debug('trimAll() called')
    debug(`@toTrim = ${JSON.stringify(this.toTrim)}`)

    const trimByType = (type, callback) => {
      const trimByID = (id, callback) => {
        debug(`Trimming for type ${type} with ID ${id}`)
        return this.db.truncate(type, id, MAX_STREAM_LENGTH, (err) => {
          if (err) {
            debug(`Trimming for type ${type} with ID ${id} gave err: ${err}`)
            return callback(err)
          } else {
            debug(`Removing from queue for type ${type} with ID ${id}`)
            set.delete(id)
            debug(`Done trimming for type ${type} with ID ${id}`)
            return callback(null)
          }
        })
      }

      debug(`Trimming for type ${type}`)
      const set = this.toTrim.get(type)
      const ids = Array.from(set)
      debug(`ids for type ${type} = ${JSON.stringify(ids)}`)

      return async.eachLimit(ids, 16, trimByID, (err) => {
        if (err != null) {
          debug(`Err trimming for type ${type}: ${err}`)
          return callback(err)
        } else {
          debug(`Done trimming for type ${type}`)
          return callback(null)
        }
      })
    }

    debug('Trimming for all keys')

    const types = Array.from(this.toTrim.keys())

    debug(`types = ${types}`)

    return async.each(types, trimByType, callback)
  }

  periodicTrimmer () {
    return this.trimAll(err => {
      if (err) {
        console.error(err)
        return this.slackMessage('error', `${err.name}: ${err.message} (in trimmer)`)
      }
    })
  }

  periodicPrepender () {
    return this.prependAll(err => {
      if (err) {
        console.error(err)
        return this.slackMessage('error', `${err.name}: ${err.message} (in prepender)`)
      }
    })
  }

  periodicIncrementer () {
    return this.incrementAll(err => {
      if (err) {
        console.error(err)
        return this.slackMessage('error', `${err.name}: ${err.message} (in prepender)`)
      }
    })
  }

  startCustom (callback) {
    this.logQ = async.queue(EvaluationLog.create, 8)

    this.toTrim = new Map()
    this.toPrepend = new Map()
    this.toIncrement = new Map()

    const streamTypes = [
      'UserEvaluationLogStream',
      'AgentEvaluationLogStream',
      'EvaluationFeedbackStream',
      'AgentVersionFeedbackStream',
      'AgentFeedbackStream',
      'AgentVersionPerformanceStream',
      'AgentPerformanceStream'
    ]

    streamTypes.forEach(type => {
      this.toTrim.set(type, new Set())
      return this.toPrepend.set(type, new Map())
    })

    const counterTypes = [
      'monthlyUsage',
      'dailyUsage',
      'monthlyAgentUsage',
      'dailyAgentUsage'
    ]

    counterTypes.forEach(type => {
      return this.toIncrement.set(type, new Map())
    })

    // We stagger these intervals out so that we don't fire
    // all our requests at the database servers at the same time

    setTimeout(() => {
      this.trimmer = setInterval(this.periodicTrimmer, this.config.trimPeriod
        , _.random(this.config.trimPeriod))
    })

    setTimeout(() => {
      this.prepender = setInterval(this.periodicPrepender, this.config.prependPeriod
        , _.random(this.config.prependPeriod))
    })

    setTimeout(() => {
      this.incrementer = setInterval(this.periodicIncrementer, this.config.incrementPeriod
        , _.random(this.config.incrementPeriod))
    })

    return callback(null)
  }

  stopCustom (callback) {
    const waitForQueue = function (q, callback) {
      debug(`Waiting for q with length ${q.length()}`)
      if (q.idle() > 0) {
        debug('Empty queue; completing')
        return callback(null)
      } else {
        debug('Adding drain handler')
        q.drain = function () {
          debug('Queue is drained')
          return callback(null)
        }
      }
    }

    // wait for these queues to drain before shutting down

    debug('Waiting for queues')

    return async.parallel([
      callback => {
        debug(`Clearing trim interval ${this.trimmer}`)
        clearInterval(this.trimmer)
        this.trimmer = null
        debug('Trimming remaining items in queue')
        return this.trimAll(callback)
      },
      callback => {
        debug(`Clearing increment interval ${this.incrementer}`)
        clearInterval(this.incrementer)
        this.incrementer = null
        debug('Incrementing remaining items in queue')
        return this.incrementAll(callback)
      },
      callback => {
        debug(`Clearing prepend interval ${this.prepender}`)
        clearInterval(this.prepender)
        this.prepender = null
        debug('Prepending remaining items in queue')
        return this.prependAll(callback)
      },
      callback => {
        return waitForQueue(this.logQ, callback)
      }
    ], err => callback(err))
  }
}

module.exports = StatsServer

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}
