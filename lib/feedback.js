// feedback.coffee
// Copyright 2014-2017 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')
const db = require('databank')

const Feedback = db.DatabankObject.subClass('Feedback')

Feedback.schema = {
  pkey: 'id',
  fields: [
    'evaluationLog',
    'data'
  ],
  indices: ['evaluationLog']
}

Feedback.beforeCreate = function (props, callback) {
  assert(_.isString(props.id), 'Feedback has no id')
  assert(_.isString(props.evaluationLog), 'Feedback has no evaluationLog')
  assert(_.isObject(props.data), 'Feedback has no data')

  props.createdAt = (new Date()).toISOString()

  return callback(null, props)
}

module.exports = Feedback
