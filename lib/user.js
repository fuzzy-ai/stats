// user.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const db = require('databank')

const User = db.DatabankObject.subClass('user')

User.schema = {
  pkey: 'id',
  fields: [
    'email',
    'passwordHash',
    'createdAt',
    'updatedAt'
  ],
  indices: ['email']
}

User.beforeCreate = (props, callback) => callback(new Error('Read-only'))

User.prototype.beforeUpdate = (props, callback) => callback(new Error('Read-only'))

User.prototype.beforeSave = callback => callback(new Error('Read-only'))

User.prototype.beforeDel = callback => callback(new Error('Read-only'))

module.exports = User
