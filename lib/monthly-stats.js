// monthly-stats.coffee
// Copyright 2016, Fuzzy.ai <legal@fuzzy.ai>
// All rights reserved.

const assert = require('assert')

const _ = require('lodash')
const {DatabankObject} = require('databank')

const lpad = function (value, padding) {
  let zeroes = '0'
  for (let i = 1, end = padding, asc = end >= 1; asc ? i <= end : i >= end; asc ? i++ : i--) { zeroes += '0' }

  return (zeroes + value).slice(padding * -1)
}

const MonthlyStats = DatabankObject.subClass('MonthlyStats')

MonthlyStats.schema = {
  pkey: 'key',
  fields: [
    'id',
    'year',
    'month',
    'total',
    'byDay',
    'createdAt',
    'updatedAt'
  ],
  indices: ['id']
}

MonthlyStats.toKey = (id, year, month) => `${id}-${lpad(year, 4)}-${lpad(month, 2)}`

MonthlyStats.beforeCreate = function (props, callback) {
  try {
    assert(_.isString(props.id), "'id' is required")
    assert(_.isFinite(props.year), "'year' is required")
    assert(_.isFinite(props.month), "'month' is required")
    assert(_.isFinite(props.total), "'total' is required")
    assert(_.isObject(props.byDay), "'byDay' is required")
    for (const x of Array.from(props.byDay)) { assert(_.isFinite(x), "'byDay' must be all numbers") }
  } catch (err) {
    return callback(err)
  }

  props.key = MonthlyStats.toKey(props.id, props.year, props.month)
  props.updatedAt = (props.createdAt = (new Date()).toISOString())

  return callback(null, props)
}

MonthlyStats.prototype.beforeUpdate = function (props, callback) {
  props.updatedAt = (new Date()).toISOString()

  return callback(null, props)
}

MonthlyStats.prototype.beforeSave = function (callback) {
  try {
    assert(_.isString(this.id), "'id' is required")
    assert(_.isFinite(this.year), "'year' is required")
    assert(_.isFinite(this.month), "'month' is required")
    assert(_.isFinite(this.total), "'total' is required")
    assert(_.isObject(this.byDay), "'byDay' is required")
  } catch (err) {
    return callback(err)
  }

  this.updatedAt = (new Date()).toISOString()

  if ((this.createdAt == null)) {
    this.key = MonthlyStats.toKey(this.id, this.year, this.month)
    this.createdAt = this.updatedAt
  }

  return callback(null)
}

MonthlyStats.maybeGet = function (id, year, month, callback) {
  const key = MonthlyStats.toKey(id, year, month)
  return MonthlyStats.get(key, (err, stats) => {
    if (err && (err.name === 'NoSuchThingError')) {
      return callback(null, null)
    } else if (err) {
      return callback(err)
    } else {
      return callback(null, stats)
    }
  })
}

module.exports = MonthlyStats
