// evaluationlog.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const _ = require('lodash')
const db = require('databank')

const EvaluationLog = db.DatabankObject.subClass('EvaluationLog')

EvaluationLog.schema = {
  pkey: 'reqID',
  fields: [
    'userID',
    'agentID',
    'versionID',
    'input',
    'fuzzified',
    'rules',
    'inferred',
    'clipped',
    'combined',
    'centroid',
    'crisp',
    'feedback',
    'createdAt'
  ],
  indices: ['userID', 'agentID', 'versionID']
}

EvaluationLog.beforeCreate = function (props, callback) {
  if (!_.isString(props.reqID)) {
    return callback(new Error('EvaluationLog has no reqID'))
  }

  if (!_.isString(props.userID)) {
    return callback(new Error('EvaluationLog has no userID'))
  }

  if (!_.isString(props.agentID)) {
    return callback(new Error('EvaluationLog has no agentID'))
  }

  if (!_.isString(props.versionID)) {
    return callback(new Error('EvaluationLog has no versionID'))
  }

  if (!_.isObject(props.input)) {
    return callback(new Error('EvaluationLog has no input'))
  }

  if (!_.isObject(props.fuzzified)) {
    return callback(new Error('EvaluationLog has no fuzzified'))
  }

  if (!_.isObject(props.inferred)) {
    return callback(new Error('EvaluationLog has no inferred'))
  }

  if (!_.isObject(props.clipped)) {
    return callback(new Error('EvaluationLog has no clipped'))
  }

  if (!_.isObject(props.combined)) {
    return callback(new Error('EvaluationLog has no combined'))
  }

  if (!_.isObject(props.centroid)) {
    return callback(new Error('EvaluationLog has no centroid'))
  }

  if (!_.isObject(props.crisp)) {
    return callback(new Error('EvaluationLog has no crisp'))
  }

  props.createdAt = (new Date()).toISOString()

  return callback(null, props)
}

EvaluationLog.prototype.beforeUpdate = function (props, callback) {
  if (!_.isEqual(props, _.pick(props, 'feedback'))) {
    return callback(new Error('Evaluation logs are immutable'))
  }
}

EvaluationLog.prototype.beforeDel = callback => callback(new Error('Evaluation logs are immutable'))

module.exports = EvaluationLog
