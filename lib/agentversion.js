// agentversion.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const db = require('databank')

const AgentVersion = db.DatabankObject.subClass('AgentVersion')

AgentVersion.schema = {
  pkey: 'id',
  fields: [
    'userID',
    'versionOf',
    'createdAt',
    'properties'
  ],
  indices: ['userID', 'versionOf']
}

AgentVersion.beforeCreate = (props, callback) => callback(new Error('Read-only'))

AgentVersion.prototype.beforeUpdate = (props, callback) => callback(new Error('Read-only'))

AgentVersion.prototype.beforeSave = callback => callback(new Error('Read-only'))

AgentVersion.prototype.beforeDel = callback => callback(new Error('Read-only'))

module.exports = AgentVersion
