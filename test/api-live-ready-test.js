// api-get-agent-month-test.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

const env = require('./config')

vows
  .describe('GET /live and GET /ready')
  .addBatch({
    'When we start an StatsServer': {
      topic () {
        const { callback } = this
        try {
          const StatsServer = require('../lib/statsserver')
          const server = new StatsServer(env)
          server.start((err) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null, server)
            }
          })
        } catch (error) {
          const err = error
          callback(err)
        }
        return undefined
      },
      'it works' (err, server) {
        return assert.ifError(err)
      },
      'teardown' (server) {
        const { callback } = this
        server.stop(err => {
          console.error(err)
          callback(null)
        })
        return undefined
      },
      'and we get the liveness endpoint': {
        topic (server) {
          request.get('http://localhost:2342/live', this.callback)
          return undefined
        },
        'it works' (err, response, body) {
          assert.ifError(err)
          assert.equal(response.statusCode, 200)
          assert.isString(body)
          const obj = JSON.parse(body)
          assert.isObject(obj)
          return assert.equal(obj.status, 'OK')
        }
      },
      'and we get the readiness endpoint': {
        topic (server) {
          request.get('http://localhost:2342/ready', this.callback)
          return undefined
        },
        'it works' (err, response, body) {
          assert.ifError(err)
          assert.equal(response.statusCode, 200)
          assert.isString(body)
          const obj = JSON.parse(body)
          assert.isObject(obj)
          return assert.equal(obj.status, 'OK')
        }
      }
    }}).export(module)
