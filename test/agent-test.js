// agent-test.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank
const async = require('async')

const env = require('./config')

vows
  .describe('Agent')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const Agent = require('../lib/agent')
          const AgentVersion = require('../lib/agentversion')
          callback(null, Agent, AgentVersion)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, Agent, AgentVersion) {
        assert.ifError(err)
        return assert.isFunction(Agent)
      },
      'and we set up the database': {
        topic (Agent, AgentVersion) {
          const { callback } = this
          const params = env['PARAMS']
          params.schema = {
            Agent: Agent.schema,
            AgentVersion: AgentVersion.schema
          }
          const db = Databank.get(env['DRIVER'], params)
          db.connect({}, (err) => {
            if (err) {
              return callback(err)
            } else {
              DatabankObject.bank = db
              return callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          return assert.ifError(err)
        },
        'and we get a new Agent': {
          topic (Agent) {
            const db = Agent.bank()
            const now = (new Date()).toISOString()
            const id = Date.now().toString()
            async.waterfall([
              function (callback) {
                const props = {
                  id: `VERSION${id}`,
                  userID: `USER${id}`,
                  versionOf: `AGENT${id}`,
                  createdAt: now,
                  properties: {
                    inputs: {
                      temperature: {
                        cold: [50, 75],
                        normal: [50, 75, 85, 100],
                        hot: [85, 100]
                      }
                    },
                    outputs: {
                      fanSpeed: {
                        slow: [50, 100],
                        normal: [50, 100, 150, 200],
                        fast: [150, 200]
                      }
                    },
                    rules: [
                      'IF temperature IS cold THEN fanSpeed IS slow',
                      'IF temperature IS normal THEN fanSpeed IS normal',
                      'IF temperature IS hot THEN fanSpeed IS fast'
                    ]
                  }
                }
                return db.create('AgentVersion', props.id, props, callback)
              },
              function (version, callback) {
                const props = {
                  id: `AGENT${id}`,
                  userID: `USER${id}`,
                  latestVersion: version.id,
                  name: 'Unit test agent',
                  createdAt: now,
                  updatedAt: now
                }
                return db.create('Agent', props.id, props, callback)
              },
              (created, callback) => Agent.get(created.id, callback)
            ], this.callback)
            return undefined
          },
          'it works' (err, agent) {
            assert.ifError(err)
            return assert.isObject(agent)
          },
          'and we try to update it': {
            topic (agent) {
              const { callback } = this
              agent.update({email: 'notdefault@mail.localhost'}, (err, updated) => {
                if (err) {
                  return callback(null)
                } else {
                  return callback(new Error('Unexpected success'))
                }
              })
              return undefined
            },
            'it fails correctly' (err) {
              return assert.ifError(err)
            },
            'and we try to save it': {
              topic (agent) {
                const { callback } = this
                agent.email = 'notdefault@mail.localhost'
                agent.save((err) => {
                  if (err) {
                    return callback(null)
                  } else {
                    return callback(new Error('Unexpected success'))
                  }
                })
                return undefined
              },
              'it fails correctly' (err) {
                return assert.ifError(err)
              },
              'and we try to delete it': {
                topic (agent) {
                  const { callback } = this
                  agent.del((err) => {
                    if (err) {
                      return callback(null)
                    } else {
                      return callback(new Error('Unexpected success'))
                    }
                  })
                  return undefined
                },
                'it fails correctly' (err) {
                  return assert.ifError(err)
                }
              }
            }
          }
        }
      }
    }}).export(module)
