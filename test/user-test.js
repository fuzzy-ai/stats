// user-test.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank
const async = require('async')

const env = require('./config')

vows
  .describe('User')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const User = require('../lib/user')
          callback(null, User)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, User) {
        assert.ifError(err)
        return assert.isFunction(User)
      },
      'and we set up the database': {
        topic (User) {
          const { callback } = this
          const params = env['PARAMS']
          params.schema =
            {User: User.schema}
          const db = Databank.get(env['DRIVER'], params)
          db.connect({}, (err) => {
            if (err) {
              return callback(err)
            } else {
              DatabankObject.bank = db
              return callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          return assert.ifError(err)
        },
        'and we get a new User': {
          topic (User) {
            const db = User.bank()
            const now = (new Date()).toISOString()
            const id = Date.now().toString()
            async.waterfall([
              function (callback) {
                const props = {
                  id,
                  email: 'fakename@mail.localhost',
                  passwordHash: 'foo',
                  createdAt: now,
                  updatedAt: now
                }
                return db.create('user', id, props, callback)
              },
              (created, callback) => User.get(created.id, callback)
            ], this.callback)
            return undefined
          },
          'it works' (err, user) {
            assert.ifError(err)
            return assert.isObject(user)
          },
          'and we try to update it': {
            topic (user) {
              const { callback } = this
              user.update({email: 'notdefault@mail.localhost'}, (err, updated) => {
                if (err) {
                  return callback(null)
                } else {
                  return callback(new Error('Unexpected success'))
                }
              })
              return undefined
            },
            'it fails correctly' (err) {
              return assert.ifError(err)
            },
            'and we try to save it': {
              topic (user) {
                const { callback } = this
                user.email = 'notdefault@mail.localhost'
                user.save((err) => {
                  if (err) {
                    return callback(null)
                  } else {
                    return callback(new Error('Unexpected success'))
                  }
                })
                return undefined
              },
              'it fails correctly' (err) {
                return assert.ifError(err)
              },
              'and we try to delete it': {
                topic (user) {
                  const { callback } = this
                  user.del((err) => {
                    if (err) {
                      return callback(null)
                    } else {
                      return callback(new Error('Unexpected success'))
                    }
                  })
                  return undefined
                },
                'it fails correctly' (err) {
                  return assert.ifError(err)
                }
              }
            }
          }
        }
      }
    }}).export(module)
