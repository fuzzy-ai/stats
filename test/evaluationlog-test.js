// evaluationlog-test.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank
const async = require('async')

const env = require('./config')

vows
  .describe('EvaluationLog')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const EvaluationLog = require('../lib/evaluationlog')
          callback(null, EvaluationLog)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, EvaluationLog) {
        assert.ifError(err)
        return assert.isFunction(EvaluationLog)
      },
      'and we set up the database': {
        topic (EvaluationLog) {
          const { callback } = this
          const params = env['PARAMS']
          params.schema =
            {EvaluationLog: EvaluationLog.schema}
          const db = Databank.get(env['DRIVER'], params)
          db.connect({}, (err) => {
            if (err) {
              return callback(err)
            } else {
              DatabankObject.bank = db
              return callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          return assert.ifError(err)
        },
        'and we get a new EvaluationLog': {
          topic (EvaluationLog) {
            const db = EvaluationLog.bank()
            const now = (new Date()).toISOString()
            const id = Date.now().toString()
            async.waterfall([
              function (callback) {
                const props = {
                  reqID: id,
                  userID: 'aaaabbbbcccc',
                  agentID: 'ddddeeeeffff',
                  versionID: '444455556666',
                  input: {
                    temperature: 60,
                    pressure: 750
                  },
                  fuzzified: {
                    temperature: {
                      low: 0.6,
                      medium: 0.4,
                      high: 0.0
                    },
                    pressure: {
                      low: 0.3,
                      medium: 0.7,
                      high: 0.0
                    }
                  },
                  inferred: {
                    windChill: {
                      high: 0.3,
                      medium: 0.1,
                      low: 0.0
                    }
                  },
                  clipped: {
                    windChill: {
                      high: [[8, 0], [10, 0.3], [12, 0.3], [14, 0]],
                      medium: [[4, 0], [6, 0.1], [8, 0.1], [10, 0]],
                      low: [[0, 0], [1, 0], [2, 0], [3, 0]]
                    }
                  },
                  combined: {
                    windChill: [[4, 0], [6, 0.1], [8, 0.1], [10, 0.3], [12, 0.3], [14, 0]]
                  },
                  centroid: {
                    windChill: [9, 0.2]
                  },
                  crisp: {
                    windChill: 9
                  },
                  createdAt: now
                }
                return db.create('EvaluationLog', id, props, callback)
              },
              (created, callback) => EvaluationLog.get(id, callback)
            ], this.callback)
            return undefined
          },
          'it works' (err, log) {
            assert.ifError(err)
            assert.isObject(log)
            assert.isString(log.reqID)
            return assert.isUndefined(log._id)
          },
          'and we try to update it': {
            topic (log) {
              const { callback } = this
              log.update({agentID: 'gggghhhhiiii'}, (err, updated) => {
                if (err) {
                  return callback(null)
                } else {
                  return callback(new Error('Unexpected success'))
                }
              })
              return undefined
            },
            'it fails correctly' (err) {
              return assert.ifError(err)
            },
            'and we try to delete it': {
              topic (log) {
                const { callback } = this
                log.del((err) => {
                  if (err) {
                    return callback(null)
                  } else {
                    return callback(new Error('Unexpected success'))
                  }
                })
                return undefined
              },
              'it fails correctly' (err) {
                return assert.ifError(err)
              }
            }
          }
        }
      }
    }}).export(module)
