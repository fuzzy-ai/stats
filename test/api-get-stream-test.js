// api-get-stream-test.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const EventEmitter = require('events')

const vows = require('perjury')
const { assert } = vows
const request = require('request')
const async = require('async')
const _ = require('lodash')

const env = require('./config')

EventEmitter.defaultMaxListeners = 1001

vows
  .describe('GET /user/:userID/stream')
  .addBatch({
    'When we start an StatsServer': {
      topic () {
        const { callback } = this
        try {
          const StatsServer = require('../lib/statsserver')
          const server = new StatsServer(env)
          server.start((err) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null, server)
            }
          })
        } catch (error) {
          const err = error
          callback(err)
        }
        return undefined
      },
      'it works' (err, server) {
        return assert.ifError(err)
      },
      'teardown' (server) {
        const { callback } = this
        server.stop(err => {
          console.error(err)
          callback(null)
        })
        return undefined
      },
      'and we create some sample data': {
        topic (server) {
          const { callback } = this
          const data = {}
          const now = (new Date()).toISOString()
          const id = Date.now().toString()
          const { db } = server
          // Note: these only work because we've already started the server
          // and it sets up the shared DB at DatabankObject.bank
          async.waterfall([
            function (callback) {
              const userID = `USER${id}`
              const props = {
                id: userID,
                email: 'fakename@mail.localhost',
                passwordHash: 'foo',
                createdAt: now,
                updatedAt: now
              }
              return db.create('user', userID, props, callback)
            },
            function (user, callback) {
              data.user = user
              const props = {
                id: `VERSION${id}`,
                userID: `USER${id}`,
                versionOf: `AGENT${id}`,
                createdAt: now,
                properties: {
                  inputs: {
                    temperature: {
                      cold: [50, 75],
                      normal: [50, 75, 85, 100],
                      hot: [85, 100]
                    }
                  },
                  outputs: {
                    fanSpeed: {
                      slow: [50, 100],
                      normal: [50, 100, 150, 200],
                      fast: [150, 200]
                    }
                  },
                  rules: [
                    'IF temperature IS cold THEN fanSpeed IS slow',
                    'IF temperature IS normal THEN fanSpeed IS normal',
                    'IF temperature IS hot THEN fanSpeed IS fast'
                  ]
                }
              }
              return db.create('AgentVersion', props.id, props, callback)
            },
            function (version, callback) {
              data.version = version
              const props = {
                id: `AGENT${id}`,
                userID: `USER${id}`,
                latestVersion: version.id,
                name: 'Unit test agent',
                createdAt: now,
                updatedAt: now
              }
              return db.create('Agent', props.id, props, callback)
            },
            function (agent, callback) {
              data.agent = agent
              const createEvaluationLog = function (i, callback) {
                const reqID = `EVALLOG-${id}-${i}`
                const props = {
                  reqID,
                  userID: data.user.id,
                  agentID: data.agent.id,
                  versionID: data.version.id,
                  input: {
                    temperature: 60,
                    pressure: 750
                  },
                  fuzzified: {
                    temperature: {
                      low: 0.6,
                      medium: 0.4,
                      high: 0.0
                    },
                    pressure: {
                      low: 0.3,
                      medium: 0.7,
                      high: 0.0
                    }
                  },
                  inferred: {
                    windChill: {
                      high: 0.3,
                      medium: 0.1,
                      low: 0.0
                    }
                  },
                  clipped: {
                    windChill: {
                      high: [[8, 0], [10, 0.3], [12, 0.3], [14, 0]],
                      medium: [[4, 0], [6, 0.1], [8, 0.1], [10, 0]],
                      low: [[0, 0], [1, 0], [2, 0], [3, 0]]
                    }
                  },
                  combined: {
                    windChill: [[4, 0], [6, 0.1], [8, 0.1], [10, 0.3], [12, 0.3], [14, 0]]
                  },
                  centroid: {
                    windChill: [9, 0.2]
                  },
                  crisp: {
                    windChill: 9
                  }
                }
                return db.create('EvaluationLog', reqID, props, callback)
              }
              return async.times(100, createEvaluationLog, callback)
            },
            function (logs, callback) {
              data.logs = logs
              const ids = _.map(logs, 'reqID')
              return async.parallel([
                callback => db.create('UserEvaluationLogStream', data.user.id, ids, callback),
                callback => db.create('AgentEvaluationLogStream', data.agent.id, ids, callback)
              ], callback)
            }
          ], (err, streams) => {
            if (err) {
              return callback(err)
            } else {
              [data.stream] = streams
              return callback(null, data)
            }
          })
          return undefined
        },
        'it works' (err, data) {
          assert.ifError(err)
          assert.isObject(data)
          assert.isObject(data.user)
          assert.isObject(data.version)
          assert.isObject(data.agent)
          assert.isArray(data.logs)
          _.each(data.logs, log => assert.isObject(log))
          return assert.isArray(data.stream)
        },
        'and we request the user stream from the server': {
          topic (data) {
            const { callback } = this
            const { user } = data
            const options =
              {url: `http://localhost:2342/user/${user.id}/stream`}
            request.get(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 200) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null, JSON.parse(body))
              }
            })
            return undefined
          },
          'it works' (err, stream) {
            return assert.ifError(err)
          },
          'it looks correct' (err, stream) {
            assert.ifError(err)
            assert.isArray(stream)
            assert.lengthOf(stream, 20)
            return _.each(stream, (log) => {
              assert.isObject(log)
              assert.isString(log.reqID)
              return assert.isUndefined(log._id)
            })
          }
        },
        'and we request the agent stream from the server': {
          topic (data) {
            const { callback } = this
            const { agent } = data
            const options =
              {url: `http://localhost:2342/agent/${agent.id}/stream`}
            request.get(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 200) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null, JSON.parse(body))
              }
            })
            return undefined
          },
          'it works' (err, stream) {
            return assert.ifError(err)
          },
          'it looks correct' (err, stream) {
            assert.ifError(err)
            assert.isArray(stream)
            assert.lengthOf(stream, 20)
            return _.each(stream, (log) => {
              assert.isObject(log)
              assert.isString(log.reqID)
              return assert.isUndefined(log._id)
            })
          }
        }
      }
    }}).export(module)
