// monthly-stats-test.coffee
// Tests for the MonthlyStats type
// Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
// All rights reserved

const vows = require('perjury')
const { assert } = vows
const {Databank} = require('databank')

const env = require('./config')

const lpad = function (value, padding) {
  let zeroes = '0'
  for (let i = 1, end = padding, asc = end >= 1; asc ? i <= end : i >= end; asc ? i++ : i--) { zeroes += '0' }

  return (zeroes + value).slice(padding * -1)
}

const assertValidStats = function (stats) {
  assert.isObject(stats)
  assert.isString(stats.id)
  assert.isNumber(stats.year)
  assert.isNumber(stats.month)
  assert.isNumber(stats.total)
  assert.isObject(stats.byDay)
  return (() => {
    const result = []
    for (const date in stats.byDay) {
      const count = stats.byDay[date]
      assert.isString(date)
      result.push(assert.isNumber(count))
    }
    return result
  })()
}

vows.describe('MonthlyStats')
  .addBatch({
    'When we require the module': {
      topic () {
        try {
          const MonthlyStats = require('../lib/monthly-stats')
          this.callback(null, MonthlyStats)
        } catch (err) {
          this.callback(err)
        }
        return undefined
      },
      'it works' (err, MonthlyStats) {
        return assert.ifError(err)
      },
      'it is a class' (err, MonthlyStats) {
        assert.ifError(err)
        return assert.isFunction(MonthlyStats)
      },
      'and we set up the database': {
        topic (MonthlyStats) {
          const { callback } = this
          const params = JSON.parse(env['PARAMS'])
          params.schema =
            {MonthlyStats: MonthlyStats.schema}
          const db = Databank.get(env['DRIVER'], params)
          db.connect({}, (err) => {
            if (err) {
              return callback(err)
            } else {
              MonthlyStats.bank = () => db
              return callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          return assert.ifError(err)
        },
        'and we create a new instance': {
          topic (MonthlyStats) {
            try {
              const year = 2016
              const month = 5

              const props = {
                id: 'USER1',
                year,
                month,
                total: 31,
                byDay: {}
              }

              for (let day = 1; day <= 31; day++) {
                const key = `${lpad(year, 4)}-${lpad(month, 2)}-${lpad(day, 2)}`
                props.byDay[key] = 1
              }

              const stats = new MonthlyStats(props)
              this.callback(null, stats)
            } catch (err) {
              this.callback(err)
            }
            return undefined
          },
          'it works' (err, stats) {
            assert.ifError(err)
            return assertValidStats(stats)
          },
          'and we save the stats object': {
            topic (stats) {
              stats.save(this.callback)
              return undefined
            },
            'it works' (err, stats) {
              assert.ifError(err)
              assertValidStats(stats)
              assert.isString(stats.createdAt)
              return assert.isString(stats.updatedAt)
            },
            'and we try to get it': {
              topic (saved, stats, MonthlyStats) {
                MonthlyStats.maybeGet('USER1', 2016, 5, this.callback)
                return undefined
              },
              'it works' (err, stats) {
                assert.ifError(err)
                assertValidStats(stats)
                assert.isString(stats.createdAt)
                return assert.isString(stats.updatedAt)
              }
            }
          }
        },
        'and we try to get a nonexistent instance': {
          topic (MonthlyStats) {
            MonthlyStats.maybeGet('NOTAUSER1', 2016, 5, (err, stats) => {
              if (err) {
                return this.callback(err)
              } else if ((stats == null)) {
                return this.callback(null)
              } else {
                return this.callback(new Error('Unexpected success'))
              }
            })
            return undefined
          },
          'it fails correctly' (err) {
            return assert.ifError(err)
          }
        }
      }
    }}).export(module)
