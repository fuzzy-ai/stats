// api-post-evaluation-test.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')
const async = require('async')
const databank = require('databank')
const { DatabankObject } = databank
const uuid = require('uuid')
const env = require('./config')

const lpad = function (value, padding) {
  let zeroes = '0'
  for (let i = 1, end = padding, asc = end >= 1; asc ? i <= end : i >= end; asc ? i++ : i--) { zeroes += '0' }

  return (zeroes + value).slice(padding * -1)
}

const statsToday = function (id, callback) {
  const now = new Date()
  const year = now.getUTCFullYear()
  const month = now.getUTCMonth() + 1

  const options =
    {url: `http://localhost:2342/user/${id}/${lpad(year, 4)}/${lpad(month, 2)}`}

  return request.get(options, (err, response, stats) => {
    if (err) {
      return callback(err)
    } else if (response.statusCode !== 200) {
      return callback(new Error(`Bad status code ${response.statusCode}`))
    } else {
      return callback(null, JSON.parse(stats))
    }
  })
}

const getStream = function (id, callback) {
  const options =
    {url: `http://localhost:2342/user/${id}/stream`}

  return request.get(options, (err, response, body) => {
    if (err) {
      return callback(err)
    } else if (response.statusCode !== 200) {
      return callback(new Error(`Bad status code ${response.statusCode}`))
    } else {
      return callback(null, JSON.parse(body))
    }
  })
}

vows
  .describe('POST /evaluation scaling')
  .addBatch({
    'When we start an StatsServer': {
      topic () {
        const { callback } = this
        try {
          const StatsServer = require('../lib/statsserver')
          // Make periodic stuff happen faster
          env.PERIOD = 1000
          const server = new StatsServer(env)
          server.start((err) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null, server)
            }
          })
        } catch (error) {
          const err = error
          callback(err)
        }
        return undefined
      },
      'it works' (err, server) {
        return assert.ifError(err)
      },
      'teardown' (server) {
        const { callback } = this
        server.stop(err => {
          console.error(err)
          callback(null)
        })
        return undefined
      },
      'and we create some sample data': {
        topic () {
          const { callback } = this
          const data = {}
          const now = (new Date()).toISOString()
          const id = Date.now().toString()
          const db = DatabankObject.bank
          // Note: these only work because we've already started the server
          // and it sets up the shared DB at DatabankObject.bank
          async.waterfall([
            function (callback) {
              const userID = `USER${id}`
              const props = {
                id: userID,
                email: 'fakename@mail.localhost',
                passwordHash: 'foo',
                createdAt: now,
                updatedAt: now
              }
              return db.create('user', userID, props, callback)
            },
            function (user, callback) {
              data.user = user
              const props = {
                id: `VERSION${id}`,
                userID: `USER${id}`,
                versionOf: `AGENT${id}`,
                createdAt: now,
                properties: {
                  inputs: {
                    temperature: {
                      cold: [50, 75],
                      normal: [50, 75, 85, 100],
                      hot: [85, 100]
                    }
                  },
                  outputs: {
                    fanSpeed: {
                      slow: [50, 100],
                      normal: [50, 100, 150, 200],
                      fast: [150, 200]
                    }
                  },
                  rules: [
                    'IF temperature IS cold THEN fanSpeed IS slow',
                    'IF temperature IS normal THEN fanSpeed IS normal',
                    'IF temperature IS hot THEN fanSpeed IS fast'
                  ]
                }
              }
              return db.create('AgentVersion', props.id, props, callback)
            },
            function (version, callback) {
              data.version = version
              const props = {
                id: `AGENT${id}`,
                userID: `USER${id}`,
                latestVersion: version.id,
                name: 'Unit test agent',
                createdAt: now,
                updatedAt: now
              }
              return db.create('Agent', props.id, props, callback)
            }
          ], (err, agent) => {
            if (err) {
              return callback(err)
            } else {
              data.agent = agent
              return callback(null, data)
            }
          })
          return undefined
        },
        'it works' (err, data) {
          assert.ifError(err)
          assert.isObject(data)
          assert.isObject(data.user)
          assert.isObject(data.version)
          return assert.isObject(data.agent)
        },
        'and we post a lot of evaluation logs to the server': {
          topic (data) {
            const { callback } = this
            const postLog = function (i, callback) {
              const reqID = uuid.v4()
              const props = {
                reqID,
                userID: data.user.id,
                agentID: data.agent.id,
                versionID: data.version.id,
                input: {
                  temperature: 60,
                  pressure: 750
                },
                fuzzified: {
                  temperature: {
                    low: 0.6,
                    medium: 0.4,
                    high: 0.0
                  },
                  pressure: {
                    low: 0.3,
                    medium: 0.7,
                    high: 0.0
                  }
                },
                inferred: {
                  windChill: {
                    high: 0.3,
                    medium: 0.1,
                    low: 0.0
                  }
                },
                clipped: {
                  windChill: {
                    high: [[8, 0], [10, 0.3], [12, 0.3], [14, 0]],
                    medium: [[4, 0], [6, 0.1], [8, 0.1], [10, 0]],
                    low: [[0, 0], [1, 0], [2, 0], [3, 0]]
                  }
                },
                combined: {
                  windChill: [
                    [4, 0],
                    [6, 0.1],
                    [8, 0.1],
                    [10, 0.3],
                    [12, 0.3],
                    [14, 0]
                  ]
                },
                centroid: {
                  windChill: [9, 0.2]
                },
                crisp: {
                  windChill: 9
                }
              }

              const options = {
                url: 'http://localhost:2342/evaluation',
                json: props
              }

              return request.post(options, (err, response, log) => {
                if (err) {
                  return callback(err)
                } else if (response.statusCode !== 200) {
                  return callback(new Error(`Bad status code ${response.statusCode}`))
                } else {
                  return callback(null, log)
                }
              })
            }

            async.times(1000, postLog, callback)

            return undefined
          },

          'it works' (err, logs) {
            assert.ifError(err)
            assert.isArray(logs)
            return (() => {
              const result = []
              for (const log of Array.from(logs)) {
                assert.isObject(log)
                assert.isString(log.reqID)
                assert.isObject(log.input)
                assert.isObject(log.fuzzified)
                assert.isObject(log.inferred)
                assert.isObject(log.clipped)
                assert.isObject(log.combined)
                assert.isObject(log.centroid)
                result.push(assert.isObject(log.crisp))
              }
              return result
            })()
          },

          'and we wait': {
            topic () {
              const doneWaiting = () => {
                return this.callback(null)
              }
              setTimeout(doneWaiting, 5000)
              return undefined
            },
            'it works' (err) {
              return assert.ifError(err)
            },
            'and we read the monthly stats': {

              topic (logs, data) {
                const { callback } = this

                statsToday(data.user.id, callback)

                return undefined
              },

              'it works' (err, stats) {
                assert.ifError(err)
                assert.isObject(stats)
                assert.isNumber(stats.total)
                assert.isObject(stats.byDay)
              },

              // assert.equal stats.byDay[today], 1000

              'and we read the monthly stats again': {

                topic (stats, logs, data) {
                  const { callback } = this

                  statsToday(data.user.id, callback)

                  return undefined
                },

                'it works' (err, stats) {
                  assert.ifError(err)
                  assert.isObject(stats)
                  assert.isNumber(stats.total)
                  assert.isObject(stats.byDay)
                }
              }
            },

            // assert.equal stats.byDay[today], 1000

            'and we read the stream': {

              topic (logs, data) {
                getStream(data.user.id, this.callback)

                return undefined
              },

              'it works' (err, stream) {
                assert.ifError(err)
                assert.isArray(stream)
                return (() => {
                  const result = []
                  for (const log of Array.from(stream)) {
                    assert.isObject(log)
                    result.push(assert.isString(log.reqID))
                  }
                  return result
                })()
              },

              'and we read the stream again': {

                topic (last, logs, data) {
                  getStream(data.user.id, this.callback)

                  return undefined
                },

                'it works' (err, stream) {
                  assert.ifError(err)
                  assert.isArray(stream)
                  return (() => {
                    const result = []
                    for (const log of Array.from(stream)) {
                      assert.isObject(log)
                      result.push(assert.isString(log.reqID))
                    }
                    return result
                  })()
                }
              }
            }
          }
        }
      }
    }}).export(module)
