// agentversion-test.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank
const async = require('async')

const env = require('./config')

vows
  .describe('AgentVersion')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const AgentVersion = require('../lib/agentversion')
          callback(null, AgentVersion)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, AgentVersion) {
        assert.ifError(err)
        return assert.isFunction(AgentVersion)
      },
      'and we set up the database': {
        topic (AgentVersion) {
          const { callback } = this
          const params = env['PARAMS']
          params.schema =
            {AgentVersion: AgentVersion.schema}
          const db = Databank.get(env['DRIVER'], params)
          db.connect({}, (err) => {
            if (err) {
              return callback(err)
            } else {
              DatabankObject.bank = db
              return callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          return assert.ifError(err)
        },
        'and we get a new AgentVersion': {
          topic (AgentVersion) {
            const db = AgentVersion.bank()
            const now = (new Date()).toISOString()
            const id = Date.now().toString()
            async.waterfall([
              function (callback) {
                const props = {
                  id: `VERSION${id}`,
                  userID: `USER${id}`,
                  versionOf: `AGENT${id}`,
                  createdAt: now,
                  properties: {
                    inputs: {
                      temperature: {
                        cold: [50, 75],
                        normal: [50, 75, 85, 100],
                        hot: [85, 100]
                      }
                    },
                    outputs: {
                      fanSpeed: {
                        slow: [50, 100],
                        normal: [50, 100, 150, 200],
                        fast: [150, 200]
                      }
                    },
                    rules: [
                      'IF temperature IS cold THEN fanSpeed IS slow',
                      'IF temperature IS normal THEN fanSpeed IS normal',
                      'IF temperature IS hot THEN fanSpeed IS fast'
                    ]
                  }
                }
                return db.create('AgentVersion', props.id, props, callback)
              },
              (created, callback) => AgentVersion.get(created.id, callback)
            ], this.callback)
            return undefined
          },
          'it works' (err, AgentVersion) {
            assert.ifError(err)
            return assert.isObject(AgentVersion)
          },
          'and we try to update it': {
            topic (AgentVersion) {
              const { callback } = this
              AgentVersion.update({email: 'notdefault@mail.localhost'}, (err, updated) => {
                if (err) {
                  return callback(null)
                } else {
                  return callback(new Error('Unexpected success'))
                }
              })
              return undefined
            },
            'it fails correctly' (err) {
              return assert.ifError(err)
            },
            'and we try to save it': {
              topic (AgentVersion) {
                const { callback } = this
                AgentVersion.email = 'notdefault@mail.localhost'
                AgentVersion.save((err) => {
                  if (err) {
                    return callback(null)
                  } else {
                    return callback(new Error('Unexpected success'))
                  }
                })
                return undefined
              },
              'it fails correctly' (err) {
                return assert.ifError(err)
              },
              'and we try to delete it': {
                topic (AgentVersion) {
                  const { callback } = this
                  AgentVersion.del((err) => {
                    if (err) {
                      return callback(null)
                    } else {
                      return callback(new Error('Unexpected success'))
                    }
                  })
                  return undefined
                },
                'it fails correctly' (err) {
                  return assert.ifError(err)
                }
              }
            }
          }
        }
      }
    }}).export(module)
