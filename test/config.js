// config.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const env = {
  PORT: '2342',
  DRIVER: 'memory',
  PARAMS: '{}',
  LOG_FILE: '/dev/null',
  PERIOD: 500
}

module.exports = env
