// statsserver-test.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')

vows
  .describe('Stats server')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const StatsServer = require('../lib/statsserver')
          callback(null, StatsServer)
        } catch (err) {
          callback(err)
        }
        return undefined
      },
      'it works' (err, StatsServer) {
        return assert.ifError(err)
      },
      'it is a class' (err, StatsServer) {
        assert.ifError(err)
        return assert.isFunction(StatsServer)
      },
      'and we instantiate a StatsServer': {
        topic (StatsServer) {
          const { callback } = this
          try {
            const env = {
              PORT: '2342',
              DRIVER: 'memory',
              HOSTNAME: 'localhost',
              LOG_FILE: '/dev/null'
            }
            const server = new StatsServer(env)
            callback(null, server)
          } catch (err) {
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          return assert.ifError(err)
        },
        'it is an object' (err, server) {
          assert.ifError(err)
          return assert.isObject(server)
        },
        'it has a start() method' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
          return assert.isFunction(server.start)
        },
        'it has a stop() method' (err, server) {
          assert.ifError(err)
          assert.isObject(server)
          return assert.isFunction(server.stop)
        },
        'and we start the server': {
          topic (server) {
            const { callback } = this
            server.start((err) => {
              if (err) {
                return callback(err)
              } else {
                return callback(null)
              }
            })
            return undefined
          },
          'it works' (err) {
            return assert.ifError(err)
          },
          'and we request the version': {
            topic () {
              const { callback } = this
              const url = 'http://localhost:2342/version'
              request.get(url, (err, response, body) => {
                if (err) {
                  return callback(err)
                } else if (response.statusCode !== 200) {
                  return callback(new Error(`Bad status code ${response.statusCode}`))
                } else {
                  return callback(null, JSON.parse(body))
                }
              })
              return undefined
            },
            'it works' (err, version) {
              assert.ifError(err)
              assert.isString(version.version)
              return assert.isString(version.name)
            },
            'and we stop the server': {
              topic (version, server) {
                const { callback } = this
                server.stop((err) => {
                  if (err) {
                    return callback(err)
                  } else {
                    return callback(null)
                  }
                })
                return undefined
              },
              'it works' (err) {
                return assert.ifError(err)
              },
              'and we request the version': {
                topic () {
                  const { callback } = this
                  const url = 'http://localhost:2342/version'
                  request.get(url, (err, response, body) => {
                    if (err) {
                      return callback(null)
                    } else {
                      return callback(new Error('Unexpected success after server stop'))
                    }
                  })
                  return undefined
                },
                'it fails correctly' (err) {
                  return assert.ifError(err)
                }
              }
            }
          }
        }
      }
    }}).export(module)
