// api-get-empty-stream-test.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')
const async = require('async')

const env = require('./config')

vows
  .describe('GET /user/:userID/stream when empty')
  .addBatch({
    'When we start an StatsServer': {
      topic () {
        const { callback } = this
        try {
          const StatsServer = require('../lib/statsserver')
          const server = new StatsServer(env)
          server.start((err) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null, server)
            }
          })
        } catch (error) {
          const err = error
          callback(err)
        }
        return undefined
      },
      'it works' (err, server) {
        return assert.ifError(err)
      },
      'teardown' (server) {
        const { callback } = this
        server.stop(err => {
          console.error(err)
          callback(null)
        })
        return undefined
      },
      'and we create some sample data': {
        topic (server) {
          const { callback } = this
          const data = {}
          const now = (new Date()).toISOString()
          const id = Date.now().toString()
          const { db } = server
          // Note: these only work because we've already started the server
          // and it sets up the shared DB at DatabankObject.bank
          async.waterfall([
            function (callback) {
              const userID = `USER${id}`
              const props = {
                id: userID,
                email: 'fakename@mail.localhost',
                passwordHash: 'foo',
                createdAt: now,
                updatedAt: now
              }
              return db.create('user', userID, props, callback)
            },
            function (user, callback) {
              data.user = user
              const props = {
                id: `VERSION${id}`,
                userID: `USER${id}`,
                versionOf: `AGENT${id}`,
                createdAt: now,
                properties: {
                  inputs: {
                    temperature: {
                      cold: [50, 75],
                      normal: [50, 75, 85, 100],
                      hot: [85, 100]
                    }
                  },
                  outputs: {
                    fanSpeed: {
                      slow: [50, 100],
                      normal: [50, 100, 150, 200],
                      fast: [150, 200]
                    }
                  },
                  rules: [
                    'IF temperature IS cold THEN fanSpeed IS slow',
                    'IF temperature IS normal THEN fanSpeed IS normal',
                    'IF temperature IS hot THEN fanSpeed IS fast'
                  ]
                }
              }
              return db.create('AgentVersion', props.id, props, callback)
            },
            function (version, callback) {
              data.version = version
              const props = {
                id: `AGENT${id}`,
                userID: `USER${id}`,
                latestVersion: version.id,
                name: 'Unit test agent',
                createdAt: now,
                updatedAt: now
              }
              return db.create('Agent', props.id, props, callback)
            }
          ], (err, agent) => {
            if (err) {
              return callback(err)
            } else {
              data.agent = agent
              return callback(null, data)
            }
          })
          return undefined
        },
        'it works' (err, data) {
          assert.ifError(err)
          assert.isObject(data)
          assert.isObject(data.user)
          assert.isObject(data.version)
          return assert.isObject(data.agent)
        },
        'and we request the user stream from the server': {
          topic (data) {
            const { callback } = this
            const { user } = data
            const options =
              {url: `http://localhost:2342/user/${user.id}/stream`}
            request.get(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 200) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null, JSON.parse(body))
              }
            })
            return undefined
          },
          'it works' (err, stream) {
            return assert.ifError(err)
          },
          'it looks correct' (err, stream) {
            assert.ifError(err)
            assert.isArray(stream)
            return assert.lengthOf(stream, 0)
          }
        },
        'and we request the agent stream from the server': {
          topic (data) {
            const { callback } = this
            const { agent } = data
            const options =
              {url: `http://localhost:2342/agent/${agent.id}/stream`}
            request.get(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 200) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null, JSON.parse(body))
              }
            })
            return undefined
          },
          'it works' (err, stream) {
            return assert.ifError(err)
          },
          'it looks correct' (err, stream) {
            assert.ifError(err)
            assert.isArray(stream)
            return assert.lengthOf(stream, 0)
          }
        }
      }
    }}).export(module)
