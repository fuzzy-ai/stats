// api-post-evaluation-test.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')
const async = require('async')
const _ = require('lodash')
const databank = require('databank')
const { DatabankObject } = databank
const uuid = require('uuid')

const env = require('./config')

vows
  .describe('POST /evaluation')
  .addBatch({
    'When we start an StatsServer': {
      topic () {
        const { callback } = this
        try {
          const StatsServer = require('../lib/statsserver')
          const server = new StatsServer(env)
          server.start((err) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null, server)
            }
          })
        } catch (error) {
          const err = error
          callback(err)
        }
        return undefined
      },
      'it works' (err, server) {
        return assert.ifError(err)
      },
      'teardown' (server) {
        const { callback } = this
        server.stop(err => {
          console.error(err)
          callback(null)
        })
        return undefined
      },
      'and we create some sample data': {
        topic () {
          const { callback } = this
          const data = {}
          const now = (new Date()).toISOString()
          const id = Date.now().toString()
          const db = DatabankObject.bank
          // Note: these only work because we've already started the server
          // and it sets up the shared DB at DatabankObject.bank
          async.waterfall([
            function (callback) {
              const userID = `USER${id}`
              const props = {
                id: userID,
                email: 'fakename@mail.localhost',
                passwordHash: 'foo',
                createdAt: now,
                updatedAt: now
              }
              return db.create('user', userID, props, callback)
            },
            function (user, callback) {
              data.user = user
              const props = {
                id: `VERSION${id}`,
                userID: `USER${id}`,
                versionOf: `AGENT${id}`,
                createdAt: now,
                properties: {
                  inputs: {
                    temperature: {
                      cold: [50, 75],
                      normal: [50, 75, 85, 100],
                      hot: [85, 100]
                    }
                  },
                  outputs: {
                    fanSpeed: {
                      slow: [50, 100],
                      normal: [50, 100, 150, 200],
                      fast: [150, 200]
                    }
                  },
                  rules: [
                    'IF temperature IS cold THEN fanSpeed IS slow',
                    'IF temperature IS normal THEN fanSpeed IS normal',
                    'IF temperature IS hot THEN fanSpeed IS fast'
                  ]
                }
              }
              return db.create('AgentVersion', props.id, props, callback)
            },
            function (version, callback) {
              data.version = version
              const props = {
                id: `AGENT${id}`,
                userID: `USER${id}`,
                latestVersion: version.id,
                name: 'Unit test agent',
                createdAt: now,
                updatedAt: now
              }
              return db.create('Agent', props.id, props, callback)
            }
          ], (err, agent) => {
            if (err) {
              return callback(err)
            } else {
              data.agent = agent
              return callback(null, data)
            }
          })
          return undefined
        },
        'it works' (err, data) {
          assert.ifError(err)
          assert.isObject(data)
          assert.isObject(data.user)
          assert.isObject(data.version)
          return assert.isObject(data.agent)
        },
        'and we post an evaluation log to the server': {
          topic (data) {
            const { callback } = this
            const reqID = uuid.v4()
            const props = {
              reqID,
              userID: data.user.id,
              agentID: data.agent.id,
              versionID: data.version.id,
              input: {
                temperature: 60,
                pressure: 750
              },
              fuzzified: {
                temperature: {
                  low: 0.6,
                  medium: 0.4,
                  high: 0.0
                },
                pressure: {
                  low: 0.3,
                  medium: 0.7,
                  high: 0.0
                }
              },
              inferred: {
                windChill: {
                  high: 0.3,
                  medium: 0.1,
                  low: 0.0
                }
              },
              clipped: {
                windChill: {
                  high: [[8, 0], [10, 0.3], [12, 0.3], [14, 0]],
                  medium: [[4, 0], [6, 0.1], [8, 0.1], [10, 0]],
                  low: [[0, 0], [1, 0], [2, 0], [3, 0]]
                }
              },
              combined: {
                windChill: [[4, 0], [6, 0.1], [8, 0.1], [10, 0.3], [12, 0.3], [14, 0]]
              },
              centroid: {
                windChill: [9, 0.2]
              },
              crisp: {
                windChill: 9
              }
            }

            const options = {
              url: 'http://localhost:2342/evaluation',
              json: props
            }

            request.post(options, (err, response, log) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 200) {
                return callback(new Error(`Bad status code ${response.statusCode}`))
              } else {
                return callback(null, log)
              }
            })

            return undefined
          },

          'it works' (err, log) {
            assert.ifError(err)
            assert.isObject(log)
            assert.isString(log.reqID)
            assert.isObject(log.input)
            assert.isObject(log.fuzzified)
            assert.isObject(log.inferred)
            assert.isObject(log.clipped)
            assert.isObject(log.combined)
            assert.isObject(log.centroid)
            return assert.isObject(log.crisp)
          },
          'and we request the evaluation log from the server': {
            topic (log, data) {
              const { callback } = this
              const options =
                {url: `http://localhost:2342/evaluation/${log.reqID}`}
              request.get(options, (err, response, body) => {
                if (err) {
                  return callback(err)
                } else if (response.statusCode !== 200) {
                  return callback(new Error(`Bad status code ${response.statusCode}`))
                } else {
                  return callback(null, JSON.parse(body))
                }
              })
              return undefined
            },
            'it works' (err, log) {
              return assert.ifError(err)
            },
            'it looks correct' (err, log) {
              assert.ifError(err)
              assert.isObject(log)
              assert.isString(log.reqID)
              assert.isObject(log.input)
              assert.isObject(log.fuzzified)
              assert.isObject(log.inferred)
              assert.isObject(log.clipped)
              assert.isObject(log.combined)
              assert.isObject(log.centroid)
              return assert.isObject(log.crisp)
            }
          },
          'and we request the user stream from the server': {
            topic (log, data) {
              const { callback } = this
              const { user } = data
              const options =
                {url: `http://localhost:2342/user/${user.id}/stream`}
              request.get(options, (err, response, body) => {
                if (err) {
                  return callback(err)
                } else if (response.statusCode !== 200) {
                  return callback(new Error(`Bad status code ${response.statusCode}`))
                } else {
                  return callback(null, JSON.parse(body), log)
                }
              })
              return undefined
            },
            'it works' (err, stream, log) {
              return assert.ifError(err)
            },
            'it looks correct' (err, stream, log) {
              assert.ifError(err)
              assert.isArray(stream)
              // assert.greater stream.length, 0
              // assert _.some(stream, (ls) -> ls.reqID == log.reqID)
              assert(_.every(stream, ls => _.isString(ls.reqID)))
              return assert(_.every(stream, ls => _.isUndefined(ls._id)))
            }
          },
          'and we request the agent stream from the server': {
            topic (log, data) {
              const { callback } = this
              const { agent } = data
              const options =
                {url: `http://localhost:2342/agent/${agent.id}/stream`}
              request.get(options, (err, response, body) => {
                if (err) {
                  return callback(err)
                } else if (response.statusCode !== 200) {
                  return callback(new Error(`Bad status code ${response.statusCode}`))
                } else {
                  return callback(null, JSON.parse(body), log)
                }
              })
              return undefined
            },
            'it works' (err, stream, log) {
              return assert.ifError(err)
            },
            'it looks correct' (err, stream, log) {
              assert.ifError(err)
              assert.isArray(stream)
              // assert.greater stream.length, 0
              // assert _.some(stream, (ls) -> ls.reqID == log.reqID)
              assert(_.every(stream, ls => _.isString(ls.reqID)))
              return assert(_.every(stream, ls => _.isUndefined(ls._id)))
            }
          }
        }
      }
    }}).export(module)
