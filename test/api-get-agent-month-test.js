// api-get-agent-month-test.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('perjury')
const { assert } = vows
const request = require('request')
const async = require('async')

const env = require('./config')

const lpad = function (value, padding) {
  let zeroes = '0'
  for (let i = 1, end = padding, asc = end >= 1; asc ? i <= end : i >= end; asc ? i++ : i--) { zeroes += '0' }

  return (zeroes + value).slice(padding * -1)
}

vows
  .describe('GET /agent/:agentID/:year/:month')
  .addBatch({
    'When we start an StatsServer': {
      topic () {
        const { callback } = this
        try {
          const StatsServer = require('../lib/statsserver')
          const server = new StatsServer(env)
          server.start((err) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null, server)
            }
          })
        } catch (error) {
          const err = error
          callback(err)
        }
        return undefined
      },
      'it works' (err, server) {
        return assert.ifError(err)
      },
      'teardown' (server) {
        const { callback } = this
        server.stop(err => {
          console.error(err)
          callback(null)
        })
        return undefined
      },
      'and we create some sample data': {
        topic (server) {
          const { callback } = this
          const data = {}
          const now = (new Date()).toISOString()
          const id = Date.now().toString()
          const { db } = server
          // Note: these only work because we've already started the server
          // and it sets up the shared DB at DatabankObject.bank
          async.waterfall([
            function (callback) {
              const userID = `USER${id}`
              const props = {
                id: userID,
                email: 'fakename@mail.localhost',
                passwordHash: 'foo',
                createdAt: now,
                updatedAt: now
              }
              return db.create('user', userID, props, callback)
            },
            function (user, callback) {
              data.user = user
              const props = {
                id: `VERSION${id}`,
                userID: `USER${id}`,
                versionOf: `AGENT${id}`,
                createdAt: now,
                properties: {
                  inputs: {
                    temperature: {
                      cold: [50, 75],
                      normal: [50, 75, 85, 100],
                      hot: [85, 100]
                    }
                  },
                  outputs: {
                    fanSpeed: {
                      slow: [50, 100],
                      normal: [50, 100, 150, 200],
                      fast: [150, 200]
                    }
                  },
                  rules: [
                    'IF temperature IS cold THEN fanSpeed IS slow',
                    'IF temperature IS normal THEN fanSpeed IS normal',
                    'IF temperature IS hot THEN fanSpeed IS fast'
                  ]
                }
              }
              return db.create('AgentVersion', props.id, props, callback)
            },
            function (version, callback) {
              const props = {
                id: `AGENT${id}`,
                userID: `USER${id}`,
                latestVersion: version.id,
                name: 'Unit test agent',
                createdAt: now,
                updatedAt: now
              }
              return db.create('Agent', props.id, props, callback)
            },
            function (agent, callback) {
              data.agent = agent
              return async.parallel([
                callback => db.create('monthlyUsage', `${data.user.id}-2014-10`, 3496, callback),
                function (callback) {
                  const daily = (i, callback) => db.create('dailyUsage', `${data.user.id}-2014-10-${lpad(i, 2)}`, i + 100, callback)
                  return async.each(__range__(1, 31, true), daily, callback)
                },
                callback => db.create('monthlyAgentUsage', `${data.agent.id}-2014-10`, 1996, callback),
                function (callback) {
                  const daily = (i, callback) => db.create('dailyAgentUsage', `${data.agent.id}-2014-10-${lpad(i, 2)}`, i + 50, callback)
                  return async.each(__range__(1, 31, true), daily, callback)
                }
              ], err => callback(err))
            }
          ], (err) => {
            if (err) {
              return callback(err)
            } else {
              return callback(null, data)
            }
          })
          return undefined
        },
        'it works' (err, data) {
          assert.ifError(err)
          assert.isObject(data)
          return assert.isObject(data.user)
        },
        'and we request the monthly usage data for the agent from the server': {
          topic (data) {
            const { callback } = this
            const { agent } = data
            const options =
              {url: `http://localhost:2342/agent/${agent.id}/2014/10`}
            request.get(options, (err, response, body) => {
              if (err) {
                return callback(err)
              } else if (response.statusCode !== 200) {
                let message
                try {
                  const se = JSON.parse(body);
                  ({ message } = se)
                } catch (error) {
                  err = error
                  message = '<none>'
                }
                return callback(new Error(`Bad status code ${response.statusCode}: ${message}`))
              } else {
                return callback(null, JSON.parse(body))
              }
            })
            return undefined
          },
          'it works' (err, data) {
            return assert.ifError(err)
          },
          'it looks correct' (err, data) {
            assert.ifError(err)
            assert.isObject(data)
            assert.isNumber(data.total)
            assert.equal(data.total, 1996)
            assert.isObject(data.byDay)
            return (() => {
              const result = []
              for (let i = 1; i <= 31; i++) {
                const key = `2014-10-${lpad(i, 2)}`
                assert.isNumber(data.byDay[key], `Expected data for ${key} to be a number`)
                result.push(assert.equal(data.byDay[key], i + 50))
              }
              return result
            })()
          }
        }
      }
    }}).export(module)

function __range__ (left, right, inclusive) {
  const range = []
  const ascending = left < right
  const end = !inclusive ? right : ascending ? right + 1 : right - 1
  for (let i = left; ascending ? i < end : i > end; ascending ? i++ : i--) {
    range.push(i)
  }
  return range
}
