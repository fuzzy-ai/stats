FROM node:9-alpine

RUN apk add --no-cache curl

WORKDIR /opt/stats
COPY . /opt/stats

RUN npm install

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/opt/stats/bin/dumb-init", "--"]
CMD ["npm", "start"]
